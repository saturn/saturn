# Saturn Notebooks beyond Jupyter

This project tries to improve the (very well established) jupyter notebook by several aspects:


* smaller - pypy sandbox
* faster (pypy kernel)
* improved communication, build on gRPC
* safe to execute code, safe, encrypted communication
* yaml notebook container format for compacter files
* vue.js frontend
* SciTXT (future) markdown language

## Acknowledgments

We would like to thank Daniel Clegg for releasing his gitLab group in favor of this project ! 

## Repository Maintainer:

* mark doerr (mark.doerr@uni-greifswald.de)